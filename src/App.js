import React, { useState, useEffect } from 'react'
import axios from 'axios'
import Persons from './components/Persons'
import Filter from './components/Filter'
import PersonForm from './components/PersonForm';

const App = () => {
  const [persons, setPersons] = useState([])
  const [newName, setNewName] = useState('')
  const [newNumber, setNewNumber] = useState('')
  const [findName, setFindName] = useState('')

  const addNewPerson = (event) => {
    event.preventDefault()
    if (0 < persons.filter(person => person.name === newName).length) {
      alert(`${newName} is already in the phone book`)
      setNewName('')
      return
    }
    const newPerson = {
      name: newName,
      number: newNumber
    }
    setPersons(persons.concat(newPerson))
    setNewName('')
    setNewNumber('')
  }

  const handleNameInputChange = (event) => {
    setNewName(event.target.value)
  }

  const handleNumberInputChange = (event) => {
    setNewNumber(event.target.value)
  }

  const handleNameSearch = (event) => {
    const nameToSearch = event.target.value
    setFindName(nameToSearch)
  }

  // const personFormStates = {
  //   addNewPerson: addNewPerson,
  //   newName: newName,
  //   handleNameInputChange: handleNameInputChange,
  //   newNumber: newNumber,
  //   handleNumberInputChange: handleNumberInputChange
  // }
  const toShow = () => findName === '' ? persons : persons.filter(person => person.name.toLowerCase().includes(findName.toLowerCase()))

  useEffect(() => {
    axios.get('http://localhost:3001/persons').then(response => setPersons(response.data))
  }, []);

  return (
    <div>
      <h2>Phonebook</h2>
      <Filter value={findName} onChange={handleNameSearch} />
      {/* <PersonForm addNewPerson={addNewPerson} newName={newName} handleNameInputChange={handleNameInputChange} newNumber={newNumber} handleNumberInputChange={handleNumberInputChange} />
    <PersonForm {...personFormStates}/> */}
      <PersonForm {...{
        addNewPerson: addNewPerson,
        newName: newName,
        handleNameInputChange: handleNameInputChange,
        newNumber: newNumber,
        handleNumberInputChange: handleNumberInputChange
      }} />
      <h2>Numbers</h2>
      <Persons persons={toShow()} />
    </div>
  )
}

export default App